<?php
declare(strict_types=1);

namespace App\Service;

use App\DTO\BooksFilter;

class FilterHandle
{

    public static function booksFilter(BooksFilter $filter): array
    {
        $params = [];
        if ($filter->year) {
            $params['year'] = $filter->year;
        }
        return $params;
    }
}
