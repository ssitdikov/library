<?php
declare(strict_types=1);

namespace App\Service;

use App\Entity\AuthorEntity;
use App\Repository\AuthorEntityRepository;

class AuthorService
{
    private AuthorEntityRepository $authorRepository;

    public function __construct(AuthorEntityRepository $authorRepository)
    {
        $this->authorRepository = $authorRepository;
    }

    public function create(\stdClass $authorData): AuthorEntity
    {
        $author = $this->authorRepository->findOneBy(['name' => $authorData->name]);
        if (!$author) {
            $author = new AuthorEntity();
            $author->setName($authorData->name);
            $this->authorRepository->save($author, true);
        }
        return $author;
    }

    public function update(AuthorEntity $authorEntity): AuthorEntity
    {
        $this->authorRepository->save($authorEntity, true);
        return $authorEntity;
    }

}
