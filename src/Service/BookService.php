<?php
declare(strict_types=1);

namespace App\Service;

use App\DTO\BooksFilter;
use App\Entity\AuthorEntity;
use App\Entity\BookEntity;
use App\Event\ClearBookAuthorsEvent;
use App\Repository\BookEntityRepository;
use stdClass;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class BookService
{
    private BookEntityRepository $bookRepository;
    private EventDispatcherInterface $eventDispatcher;

    /**
     * @param BookEntityRepository $bookRepository
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(
        BookEntityRepository     $bookRepository,
        EventDispatcherInterface $eventDispatcher)
    {
        $this->bookRepository = $bookRepository;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param int $page
     * @return BookEntity[]
     */
    public function list(int $page): array
    {
        $limit = 10;
        $offset = ($page - 1) * $limit;
        return $this->bookRepository->findBy([], limit: $limit, offset: $offset);
    }

    /**
     * @param BooksFilter $filter
     * @return BookEntity[]
     */
    public function filter(BooksFilter $filter): array
    {
        switch ($filter->doubleAuthors) {
            case $filter::DOUBLE_AUTHORS_SQL:
                return $this->bookRepository->doubleAuthorsSQL();
                break;
            case $filter::DOUBLE_AUTHORS_DQB:
                return $this->bookRepository->doubleAuthorsDQB();
                break;
            default:
                return $this->bookRepository->findBy(
                    FilterHandle::booksFilter($filter),
                    null,
                    $filter->limit,
                    ($filter->page - 1) * $filter->limit
                );
        }
    }

    public function getOne(int $id): BookEntity
    {
        return $this->bookRepository->find($id);
    }

    /**
     * @param stdClass $bookData
     * @param AuthorEntity[] $authors
     * @return BookEntity
     */
    public function create(stdClass $bookData, array $authors): BookEntity
    {
        $book = new BookEntity();
        foreach ($authors as $author) {
            $book->addAuthor($author);
        }
        $book->setTitle($bookData->title)
            ->setDescription($bookData->description)
            ->setYear($bookData->year);
        if ($bookData->cover) {
            $book->setCover($bookData->cover);
        }

        $this->bookRepository->save($book, true);

        return $book;
    }

    public function update(int $id, stdClass $bookData, array $authors = []): BookEntity
    {
        $book = $this->bookRepository->find($id);
        if ($book) {
            if (isset($bookData->title)) {
                $book->setTitle($bookData->title);
            }
            if (isset($bookData->description)) {
                $book->setDescription($bookData->description);
            }
            if (isset($bookData->year)) {
                $book->setYear($bookData->year);
            }
            if (isset($bookData->cover)) {
                $book->setCover($bookData->cover);
            }
            if (!empty($authors)) {
                $this->eventDispatcher->dispatch(
                    new ClearBookAuthorsEvent($book->getAuthors())
                );
                $book->getAuthors()->clear();
                foreach ($authors as $author) {
                    $book->addAuthor($author);
                }
            }
            $this->bookRepository->save($book, true);
            return $book;
        }
        throw new NotFoundHttpException(
            sprintf('%d book not found', $id)
        );
    }

    public function delete(int $id): void
    {
        $book = $this->bookRepository->find($id);
        if ($book) {
            $this->eventDispatcher->dispatch(
                new ClearBookAuthorsEvent($book->getAuthors())
            );
            $this->bookRepository->remove(
                $book,
                true
            );
        }
    }
}
