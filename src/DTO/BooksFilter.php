<?php
declare(strict_types=1);

namespace App\DTO;

use OpenApi\Attributes\Property;

class BooksFilter
{
    public const DOUBLE_AUTHORS_SQL = 1;
    public const DOUBLE_AUTHORS_DQB = 2;

    #[Property(description: 'Count of authors for a book', type: 'integer')]
    public int $authorsNum = 1;
    #[Property(description: 'Year of publication', type: 'string', format: 'date', pattern: "^\d{4}$")]
    public int $year = 0;
    #[Property(description: 'Use specific sql query: 1 - sql, 2 - dqb', type: 'integer')]
    public int $doubleAuthors = 0;

    public int $page = 1;
    public int $limit = 10;
}
