<?php
declare(strict_types=1);

namespace App\Controller\Api;

use App\DTO\BooksFilter;
use App\Entity\BookEntity;
use App\Service\AuthorService;
use App\Service\BookService;
use Nelmio\ApiDocBundle\Annotation\Model;

use OpenApi\Attributes\Items;
use OpenApi\Attributes\JsonContent;
use OpenApi\Attributes\RequestBody;
use OpenApi\Attributes\Schema;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/book')]
class Book extends AbstractController
{

    private BookService $bookService;
    private AuthorService $authorService;

    /**
     * @param BookService $bookService
     * @param AuthorService $authorService
     */
    public function __construct(BookService $bookService, AuthorService $authorService)
    {
        $this->bookService = $bookService;
        $this->authorService = $authorService;
    }

    #[Route(
        's/{page}',
        defaults: ['page' => 1],
        methods: ['GET']
    )]
    #[\OpenApi\Attributes\Response(
        response: 200,
        description: 'Get list of books',
        content: new JsonContent(
            type: 'array',
            items: new Items(
                new Model(type: BookEntity::class)
            )
        )
    )]
    public function list(int $page): Response
    {
        return $this->json($this->bookService->list($page));
    }

    #[Route(
        's/filter',
        methods: ['POST']
    )]
    #[\OpenApi\Attributes\Response(
        response: 200,
        description: 'Get list of books',
        content: new JsonContent(
            type: 'array',
            items: new Items(
                new Model(type: BookEntity::class)
            )
        )
    )]
    public function filter(Request $request): Response
    {
        $request_json = json_decode($request->getContent(), false);
        $filter = new BooksFilter();
        $filter->year = $request_json->year ?? 0;
        $filter->authorsNum = $request_json->authorsNum ?? 1;
        $filter->doubleAuthors = $request_json->doubleAuthors ?? 0;
        $filter->page = $request_json->page ?? 1;
        $filter->limit = $request_json->limit ?? 10;
        $books = $this->bookService->filter($filter);
        return $this->json($books);
    }

    #[Route('', methods: ['POST'])]
    #[\OpenApi\Attributes\Response(
        response: 200,
        description: 'Create a book',
        content: new Model(type: BookEntity::class)
    )]
    public function add(Request $request): Response
    {
        $request_data = json_decode($request->getContent(), false);
        $authors = array_map(
            function (\stdClass $author) {
                return $this->authorService->create($author);
            },
            $request_data->authors
        );
        $book = $this->bookService->create($request_data, $authors);
        return $this->json($book->getId());
    }

    #[Route('/{id}', methods: ["GET"])]
    #[\OpenApi\Attributes\Response(
        response: 200,
        description: 'Get one book information',
        content: new Model(type: BookEntity::class)
    )]
    public function getOne(int $id): Response
    {
        $book = $this->bookService->getOne($id);
        return $this->json($book);
    }

    #[Route('/{id}', methods: ['PATCH'])]
    #[\OpenApi\Attributes\Response(
        response: 200,
        description: 'Update the book'
    )]
    public function update(int $id, Request $request): Response
    {
        $request_data = json_decode($request->getContent(), false);
        $authors = [];
        if (isset($request_data->authors)) {
            foreach ($request_data->authors as $author) {
                $authors[] = $this->authorService->create($author);
            }
        }
        $book = $this->bookService->update($id, $request_data, $authors);
        return $this->json($book->getId());
    }

    #[Route('/{id}', methods: ['DELETE'])]
    #[\OpenApi\Attributes\Response(
        response: 200,
        description: 'Delete the book'
    )]
    public function delete(int $id): Response
    {
        $this->bookService->delete($id);
        return $this->json([]);
    }
}
