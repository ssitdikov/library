<?php
declare(strict_types=1);

namespace App\Controller\Api;

use App\Entity\AuthorEntity;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/author')]
class Author extends AbstractController
{
    #[Route('/{id}')]
    #[\OpenApi\Attributes\Response(
        response: 200,
        description: 'Get one book information',
        content: new Model(type: AuthorEntity::class)
    )]
    public function getOne(int $id): Response
    {
        return $this->json([]);
    }
}
