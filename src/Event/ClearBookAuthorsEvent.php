<?php
declare(strict_types=1);

namespace App\Event;

use Doctrine\Common\Collections\Collection;

class ClearBookAuthorsEvent
{
    private Collection $authors;

    public function __construct(Collection $authors)
    {
        $this->authors = $authors;
    }

    /**
     * @return Collection
     */
    public function getAuthors(): Collection
    {
        return $this->authors;
    }
}
