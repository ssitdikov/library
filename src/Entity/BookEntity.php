<?php

namespace App\Entity;

use App\Repository\BookEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Attributes\Items;
use OpenApi\Attributes\Property;

#[ORM\Entity(repositoryClass: BookEntityRepository::class)]
#[ORM\HasLifecycleCallbacks]
class BookEntity implements \JsonSerializable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Property(description: 'The unique identifier of book')]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Property(description: 'The title of book', type: 'string', maxLength: 255)]
    private ?string $title = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Property(description: 'The description of book', type: 'string')]
    private ?string $description = null;

    #[ORM\Column]
    #[Property(description: 'Year of publication', type: 'string', format: 'date', pattern: "^\d{4}$")]
    private ?int $year = null;

    #[ORM\Column(length: 255)]
    #[Property(description: 'Main image of book', type: 'string')]
    private string $cover = '';

    #[ORM\ManyToMany(targetEntity: AuthorEntity::class, inversedBy: 'books')]
    #[Property(
        description: 'List of authors',
        type: 'array',
        items: new Items(
            new Model(
                type: AuthorEntity::class
            )
        )
    )]
    private Collection $authors;

    public function __construct()
    {
        $this->authors = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function setYear(int $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getCover(): string
    {
        return $this->cover;
    }

    public function setCover(string $cover): self
    {
        $this->cover = $cover;

        return $this;
    }

    /**
     * @return Collection<int, AuthorEntity>
     */
    public function getAuthors(): Collection
    {
        return $this->authors;
    }

    public function addAuthor(AuthorEntity $author): self
    {
        if (!$this->authors->contains($author)) {
            $this->authors->add($author);
            $author->addBook($this);
        }

        return $this;
    }

    public function removeAuthor(AuthorEntity $author): self
    {
        if ($this->authors->removeElement($author)) {
            // set the owning side to null (unless already changed)
            if ($author->getBooks()->contains($this)) {
                $author->removeBook($this);
            }
        }

        return $this;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'description' => $this->getDescription(),
            'year' => $this->getYear(),
            'cover' => $this->getCover(),
            'authors' => $this->authors->map(
                static function (AuthorEntity $author) {
                    return [
                        'id' => $author->getId(),
                        'name' => $author->getName()
                    ];
                }
            )->getValues()
        ];
    }
}
