<?php

namespace App\Entity;

use App\Repository\AuthorEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Attributes\Items;
use OpenApi\Attributes\Property;

#[ORM\Entity(repositoryClass: AuthorEntityRepository::class)]
#[ORM\HasLifecycleCallbacks]
class AuthorEntity implements \JsonSerializable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Property(description: 'The unique identifier of author')]
    private ?int $id = null;

    #[ORM\Column(length: 255, unique: true)]
    #[Property(description: 'Full name of author', type: 'string')]
    private ?string $name = null;

    #[ORM\Column]
    #[Property(description: 'Num of related books', type: 'integer')]
    private int $booksCount = 0;

    #[ORM\ManyToMany(targetEntity: BookEntity::class, mappedBy: 'authors')]
    #[Property(
        description: 'The list of books',
        type: 'array',
        items: new Items(
            new Model(
                type: BookEntity::class
            )
        )
    )]
    private Collection $books;

    public function __construct()
    {
        $this->books = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getBooksCount(): int
    {
        return $this->booksCount;
    }

    public function setBooksCount(int $booksCount): self
    {
        $this->booksCount = $booksCount;

        return $this;
    }

    /**
     * @return Collection<int, BookEntity>
     */
    public function getBooks(): Collection
    {
        return $this->books;
    }

    public function addBook(BookEntity $book): self
    {
        if (!$this->books->contains($book)) {
            $this->books->add($book);
            $book->addAuthor($this);
        }

        return $this;
    }

    public function removeBook(BookEntity $book): self
    {
        if ($this->books->removeElement($book)) {
            $book->removeAuthor($this);
        }

        return $this;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'books_count' => $this->getBooksCount(),
            'books' => $this->books->map(
                static function (BookEntity $book) {
                    return [
                        'id' => $book->getId(),
                        'title' => $book->getTitle(),
                        'year' => $book->getYear()
                    ];
                }
            )->getValues()
        ];
    }
}
