<?php
declare(strict_types=1);

namespace App\EventListener;

use App\Event\ClearBookAuthorsEvent;
use App\Service\AuthorService;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;

#[AsEventListener(event: ClearBookAuthorsEvent::class, method: 'recalculateBooksCount')]
class AuthorChangeListener
{
    private AuthorService $authorService;

    /**
     * @param AuthorService $authorService
     */
    public function __construct(AuthorService $authorService)
    {
        $this->authorService = $authorService;
    }

    public function recalculateBooksCount(ClearBookAuthorsEvent $event): void
    {
        foreach ($event->getAuthors() as $author) {
            $author->setBooksCount($author->getBooks()->count() - 1);
            $this->authorService->update($author);
        }
    }
}
