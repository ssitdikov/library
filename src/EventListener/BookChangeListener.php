<?php
declare(strict_types=1);

namespace App\EventListener;

use App\Entity\BookEntity;
use App\Service\AuthorService;
use App\Service\BookService;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use Doctrine\ORM\Event\PostPersistEventArgs;
use Doctrine\ORM\Event\PostRemoveEventArgs;
use Doctrine\ORM\Event\PostUpdateEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;

#[AsEventListener(event: Events::postUpdate)]
#[AsEventListener(event: Events::postPersist)]
#[AsEventListener(event: Events::postRemove)]
#[AsEntityListener(event: Events::postUpdate, method: 'postUpdate', entity: BookEntity::class)]
#[AsEntityListener(event: Events::postPersist, method: 'postPersist', entity: BookEntity::class)]
#[AsEntityListener(event: Events::postRemove, method: 'postRemove', entity: BookEntity::class)]
class BookChangeListener
{
    private AuthorService $authorService;

    /**
     * @param AuthorService $authorService
     */
    public function __construct(AuthorService $authorService)
    {
        $this->authorService = $authorService;
    }

    public function postUpdate(BookEntity $bookEntity, PostUpdateEventArgs $event): void
    {
        $this->triggerCount($bookEntity);
    }

    public function postPersist(BookEntity $bookEntity, PostPersistEventArgs $event): void
    {
        $this->triggerCount($bookEntity);
    }

    public function postRemove(BookEntity $bookEntity, PostRemoveEventArgs $event): void
    {
        $this->triggerCount($bookEntity);
    }

    public function triggerCount(BookEntity $entity): void
    {
        foreach ($entity->getAuthors() as $author) {
            $author->setBooksCount($author->getBooks()->count());
            $this->authorService->update($author);
        }
    }
}
